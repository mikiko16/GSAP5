import { gsap } from 'gsap/all';
import selectors from './selectors';

export default class Animation {

    constructor() {
        this._tl = gsap.timeline();

        this._tl.to(selectors.list, {
            id: 'listUp',
            y: -80
        });

        this._tl.to(selectors.list, {
            id: 'listDown',
            y: 0
        });

        this._tl.to(selectors.listItems[0], {
            id:'listItem0',
            y: 60,
            opacity: 0,               
        }).to(selectors.listItems[1], {
             id:'listItem1',
             y: 60,
             opacity: 0
        }, 1).to(selectors.listItems[2], {
            id:'listItem2',
            y: 60,
            opacity: 0
        }, 1)

        this._tl.to(selectors.truckBtnBg, {
            id: 'truckBtnScaleUp',
            scale: 1.1,
            transformOrigin: 'center'
        })

        this._tl.to(selectors.truckBtnBg, {
            id: 'truckBtnScaleDown',
            scale: 1,
            transformOrigin: 'center'
        })

        this._tl.to(selectors.container, {
            id: 'container',
            opacity: 1
        }).to(selectors.containerParts, {
            id: 'containerParts',
            opacity: 1
        }, 0)

        .addLabel('targetPoint')
        .to(selectors.backWheel1, { id: 'backWheel1', opacity: 1 }, 'targetPoint')
        .to(selectors.backWheel2, { id: 'backWheel2', opacity: 1 }, 'targetPoint')
        .to(selectors.backWheelBack1, {id: 'backWheelBack1', opacity: 1 }, 'targetPoint')
        .to(selectors.backWheelBack2, {id: 'backWheelBack2', opacity: 1 }, 'targetPoint')

        this._tl.to(selectors.frontGroup, {
            id: 'frontGroup',
            opacity: 1
        }).to(selectors.frontWheel1, {
            id: 'frontWheel1',
            opacity: 1
        }).to(selectors.frontWheel2, {
            id: 'frontWheel2',
            opacity: 1
        }, 0).to(selectors.frontWheelsBack, {
            id: 'frontWheelsBack',
            opacity: 1
        }, 0)

        this._tl.to(selectors.truck, 3, {
            id: 'truckMovement',
            x: 450,
            ease: 'back.inOut(3.7)',
            opacity: 0
        }).to(selectors.shippedLabel, {
            id: 'shippedLabel',
            opacity: 1
        })

        this._tl.pause();

        selectors.truckBtn.addEventListener('click', () => {

            if(this._tl.isActive()) {
                this._tl.restart();
            }
            else {
                this._tl.play();
            }
        })

        selectors.pauseBtn.addEventListener('click', () => {
            this._tl.pause();
        })

        selectors.playBtn.addEventListener('click', () => {
            if(this._tl.isActive()) {
                this._tl.restart();
            }
            else {
                this._tl.play();
            }
        })

        selectors.reverseBtn.addEventListener('click', () => {
            this._tl.reverse();
        })
    }


}